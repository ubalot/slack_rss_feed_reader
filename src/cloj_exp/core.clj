(ns cloj-exp.core
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))


;;; Cli management

(defn usage [options-summary]
  (->> ["Remove trailing spaces from file."
        ""
        "Usage: program-name [options] file"
        ""
        "Options:"
        options-summary
        ""
        ""
        "Please refer to the manual page for more information."]
        (clojure.string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
    (clojure.string/join \newline errors)))

(def cli-options [
  ;; A non-idempotent option (:default is applied first)
  ["-v" nil "Verbosity level"
    :id :verbosity
    :default 0
    :update-fn inc]

  ;; A boolean option defaulting to nil
  ["-h" "--help"]])

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
   should exit (with an error message, and optional ok status), or a map
   indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) ; help => exit OK with usage summary
      {:exit-message (usage summary) :ok? true}

      errors ; errors => exit with description of errors
      {:exit-message (error-msg errors)}

      ;; custom validation on arguments
      (>= (count arguments) 1)
      {:arguments arguments :options options}

      :else ; failed custom validation => exit with usage summary
      {:exit-message (usage summary)})))



;;; Actual program

(defrecord Feed [id title url])

(defn reunite-feed-by-2-line
  [text]

  (defn assign-as-last
    [items item]
    (assoc items (- (count items) 1) item))

  (defn join-elems-from-pairs
    [lines line]
    (if (clojure.string/starts-with? line "ID:")
      (conj lines line) ; append `line` at the end of `lines`
      (assign-as-last lines (clojure.string/join " - " [(last lines) line])))) ; concat to last elem of lines

  (let [lines (clojure.string/split-lines text)]
    (reduce join-elems-from-pairs [] lines)))

(defn get-feeds
  [lines]
  (for [line lines]
    (let [triple (clojure.string/split line #" - ")
          id     (clojure.string/trim (clojure.string/replace (get triple 0) #"ID:" ""))
          title  (clojure.string/trim (clojure.string/replace (get triple 1) #"Title:" ""))
          url    (clojure.string/trim (clojure.string/replace (get triple 2) #"URL:" ""))]
      (Feed. id title url))))

(defn feeds-to-text
  [feeds]

  (defn feed-to-string
    [feed]
    (str "id: " (:id feed) " title: " (:title feed) " url: " (:url feed)))

  (clojure.string/join \newline (map feed-to-string feeds)))

(defn to-json-ext
  [filename]
  (let [idx (clojure.string/last-index-of filename ".")]
    (if (nil? idx)
      filename
      (str (subs filename 0 idx) ".json"))))

(defn feeds-to-json
  [feeds]

  (defn feed-to-json
    [feed]
    (str "{" \newline
         "  \"id\": \"" (:id feed) "\"," \newline
         "  \"title\": \"" (:title feed) "\"," \newline
         "  \"url\": \"" (:url feed) "\"," \newline
         "}"))

  (defn tab-feed
    [feed]
    (clojure.string/join \newline
      (for [line (clojure.string/split-lines feed)]
        (str \tab line))))

  (defn wrap-json-feeds
    [json-feeds]
    (str "[" \newline
         (clojure.string/join (str "," \newline) json-feeds) \newline
         "["))

  (wrap-json-feeds (map tab-feed (map feed-to-json feeds))))

(defn -main
  "Entry point of the app."
  [& args]
  (let [{:keys [arguments options exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (doseq [argument arguments]
        (when (.exists (clojure.java.io/file argument))
          (let [content (slurp argument :encoding "UTF-8")
                feeds (get-feeds (reunite-feed-by-2-line content))
                text (feeds-to-json feeds)]
            (spit (to-json-ext argument) text :encoding "UTF-8")))))))
